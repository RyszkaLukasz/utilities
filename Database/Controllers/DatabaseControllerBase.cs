﻿using System;
using System.Collections.Generic;
using System.Timers;
using Npgsql;

namespace Utilities.Database
{
    public class DatabaseControllerBase
    {
        private readonly Timer _timer;
        private static readonly int _reconnectInterval = 2000;
        protected NpgsqlConnection _connection;      

        public DatabaseControllerBase()
        {
            _timer = new Timer();
            _timer.Elapsed += (sender, e) =>
            {
                _timer.Stop();
                if (OpenDatabase() != SqlStatusClass.Class_00__Successful_Completion)
                {
                    _timer.Start();
                }
            };
            _timer.Interval = _reconnectInterval;
        }

        ~DatabaseControllerBase()
        {
            Close();
        }

        public DatabaseSettings Settings { get; }

        public bool TryToOpen()
        {
            SqlStatusClass sqlStatusClass = OpenDatabase();

            switch (sqlStatusClass)
            {
                case SqlStatusClass.Class_00__Successful_Completion:
                    return true;

                default:
                    _timer.Start();
                    return false;
            }
        }

        public void Close()
        {
            _connection?.Close();
        }

        protected SqlStatusClass OpenDatabase()
        {
            try
            {
                string connstring = $"Server={Settings.IP}; Port={Settings.Port}; User Id={Settings.User}; Password={Settings.Password}; Database={Settings.Name};";
                _connection = new NpgsqlConnection(connstring);
                _connection.StateChange += new System.Data.StateChangeEventHandler((obj, args) =>
                {
                    if (args.CurrentState != System.Data.ConnectionState.Open)
                    {
                        _timer.Start();
                        Console.WriteLine("Database connection closed!");
                    }
                    else
                    {
                        Console.WriteLine("Database connection opened!");
                    }
                });
                _connection.Open();
            }
            catch (Exception msg)
            {
                if (msg is PostgresException)
                {
                    var postgresException = msg as PostgresException;
                    return SqlStatusClassExtensions.GetSqlStatusClass(postgresException.SqlState);
                }
                else
                {
                    Console.WriteLine(msg.Message);
                    return SqlStatusClass.Class_Unknow;
                }
            }

            return SqlStatusClass.Class_00__Successful_Completion;
        }
    }
}
