﻿using Npgsql;
using System;

namespace Utilities.Database
{
    public static class NpgsqlConnectionExtensions
    {
        public static NpgsqlDataReader Select(this NpgsqlConnection connection, string query)
        {
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(query, connection);
                return command.ExecuteReader();
            }
            catch (Exception e)
            {
                throw new NpgsqlException(query, e);
            }
        }

        public static void Select(this NpgsqlConnection connection, string query, Action<NpgsqlDataReader> processing)
        {
            NpgsqlDataReader dataReader = null;
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(query, connection);
                dataReader = command.ExecuteReader();
                processing(dataReader);
            }
            catch (Exception e)
            {
                throw new NpgsqlException(query, e);
            }
            finally
            {
                dataReader?.Close();
            }
        }

        public static void Insert(this NpgsqlConnection connection, string query)
        {
            try
            {
                NpgsqlCommand insert = new NpgsqlCommand(query, connection);
                insert.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new NpgsqlException(query, e);
            }
        }

        public static void Insert(this NpgsqlConnection connection, string query, Action<NpgsqlParameterCollection> preparing)
        {
            try
            {
                NpgsqlCommand insert = new NpgsqlCommand(query, connection);
                preparing(insert.Parameters);
                insert.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new NpgsqlException(query, e);
            }
        }

        public static object InsertAndReturning(this NpgsqlConnection connection, string query, Func<NpgsqlDataReader, object> returning)
        {
            try
            {
                NpgsqlCommand insert = new NpgsqlCommand(query, connection);
                return returning(insert.ExecuteReader());
            }
            catch (Exception e)
            {
                throw new NpgsqlException(query, e);
            }
        }

        public static object InsertAndReturning(this NpgsqlConnection connection, string query, Action<NpgsqlParameterCollection> preparing, Func<NpgsqlDataReader, object> returning)
        {
            try
            {
                NpgsqlCommand insert = new NpgsqlCommand(query, connection);
                preparing(insert.Parameters);
                return returning(insert.ExecuteReader());
            }
            catch (Exception e)
            {
                throw new NpgsqlException(query, e);
            }
        }

        public static void Update(this NpgsqlConnection connection, string query)
        {
            try
            {
                NpgsqlCommand update = new NpgsqlCommand(query, connection);
                update.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new NpgsqlException(query, e);
            }
        }

        public static void Update(this NpgsqlConnection connection, string query, Action<NpgsqlParameterCollection> preparing)
        {
            try
            {
                NpgsqlCommand update = new NpgsqlCommand(query, connection);
                preparing(update.Parameters);
                update.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new NpgsqlException(query, e);
            }
        }

        public static bool DoesTabelExist(this NpgsqlConnection connection, string tableName, string schemaName)
        {
            bool exists = false;

            connection.Select(
                $"SELECT 1 FROM information_schema.tables WHERE table_schema = '{schemaName.ToLower()}' AND table_name = '{tableName.ToLower()}'",
                x => exists = x.Read());
            return exists;
        }

        public static void CreateTable(this NpgsqlConnection connection, string query)
        {
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(query, connection);
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new NpgsqlException(query, e);
            }
        }

        public static void CreateSchemaIfNotExists(this NpgsqlConnection connection, string schemaName)
        {
            string query = $"CREATE SCHEMA IF NOT EXISTS {schemaName}";
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(query, connection);
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new NpgsqlException(query, e);
            }
        }
    }
}
