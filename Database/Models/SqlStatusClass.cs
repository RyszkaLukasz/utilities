﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Utilities.Database
{
    public enum SqlStatusClass
    {
        Class_Unknow,
        Class_00__Successful_Completion,
        Class_01__Warning,
        Class_02__No_Data_warning_Class_per_the_SQL_standard,
        Class_03__SQL_Statement_Not_Yet_Complete,
        Class_08__Connection_Exception,
        Class_09__Triggered_Action_Exception,
        Class_0A__Feature_Not_Supported,
        Class_0B__Invalid_Transaction_Initiation,
        Class_0F__Locator_Exception,
        Class_0L__Invalid_Grantor,
        Class_0P__Invalid_Role_Specification,
        Class_0Z__Diagnostics_Exception,
        Class_20__Case_Not_Found,
        Class_21__Cardinality_Violation,
        Class_22__Data_Exception,
        Class_23__Integrity_Constraint_Violation,
        Class_24__Invalid_Cursor_State,
        Class_25__Invalid_Transaction_State,
        Class_26__Invalid_SQL_Statement_Name,
        Class_27__Triggered_Data_Change_Violation,
        Class_28__Invalid_Authorization_Specification,
        Class_2B__Dependent_Privilege_Descriptors_Still_Exist,
        Class_2D__Invalid_Transaction_Termination,
        Class_2F__SQL_Routine_Exception,
        Class_34__Invalid_Cursor_Name,
        Class_38__External_Routine_Exception,
        Class_39__External_Routine_Invocation_Exception,
        Class_3B__Savepoint_Exception,
        Class_3D__Invalid_Catalog_Name,
        Class_3F__Invalid_Schema_Name,
        Class_40__Transaction_Rollback,
        Class_42__Syntax_Error_or_Access_Rule_Violation,
        Class_44__WITH_CHECK_OPTION_Violation,
        Class_53__Insufficient_Resources,
        Class_54__Program_Limit_Exceeded,
        Class_55__Object_Not_In_Prerequisite_State,
        Class_57__Operator_Intervention,
        Class_58__System_Error_external_to_PostgreSQL_itself,
        Class_72__Snapshot_Failure,
        Class_F0__Configuration_File_Error,
        Class_HV__Foreign_Data_Wrapper_Error_SQL_MED,
        Class_P0__PLpgSQL_Error,
        Class_XX__Internal_Error
    }

#pragma warning disable SA1649 // File name must match first type name

    public static class SqlStatusClassExtensions
#pragma warning restore SA1649 // File name must match first type name
    {
        private static readonly IDictionary<string, SqlStatusClass> _sqlStatusClassPrefix = new ReadOnlyDictionary<string, SqlStatusClass>(new Dictionary<string, SqlStatusClass>()
        {
            { "00", SqlStatusClass.Class_00__Successful_Completion },
            { "01", SqlStatusClass.Class_01__Warning },
            { "02", SqlStatusClass.Class_02__No_Data_warning_Class_per_the_SQL_standard },
            { "03", SqlStatusClass.Class_03__SQL_Statement_Not_Yet_Complete },
            { "08", SqlStatusClass.Class_08__Connection_Exception },
            { "09", SqlStatusClass.Class_09__Triggered_Action_Exception },
            { "0A", SqlStatusClass.Class_0A__Feature_Not_Supported },
            { "0B", SqlStatusClass.Class_0B__Invalid_Transaction_Initiation },
            { "0F", SqlStatusClass.Class_0F__Locator_Exception },
            { "0L", SqlStatusClass.Class_0L__Invalid_Grantor },
            { "0P", SqlStatusClass.Class_0P__Invalid_Role_Specification },
            { "0Z", SqlStatusClass.Class_0Z__Diagnostics_Exception },
            { "20", SqlStatusClass.Class_20__Case_Not_Found },
            { "21", SqlStatusClass.Class_21__Cardinality_Violation },
            { "22", SqlStatusClass.Class_22__Data_Exception },
            { "23", SqlStatusClass.Class_23__Integrity_Constraint_Violation },
            { "24", SqlStatusClass.Class_24__Invalid_Cursor_State },
            { "25", SqlStatusClass.Class_25__Invalid_Transaction_State },
            { "26", SqlStatusClass.Class_26__Invalid_SQL_Statement_Name },
            { "27", SqlStatusClass.Class_27__Triggered_Data_Change_Violation },
            { "28", SqlStatusClass.Class_28__Invalid_Authorization_Specification },
            { "2B", SqlStatusClass.Class_2B__Dependent_Privilege_Descriptors_Still_Exist },
            { "2D", SqlStatusClass.Class_2D__Invalid_Transaction_Termination },
            { "2F", SqlStatusClass.Class_2F__SQL_Routine_Exception },
            { "34", SqlStatusClass.Class_34__Invalid_Cursor_Name },
            { "38", SqlStatusClass.Class_38__External_Routine_Exception },
            { "39", SqlStatusClass.Class_39__External_Routine_Invocation_Exception },
            { "3B", SqlStatusClass.Class_3B__Savepoint_Exception },
            { "3D", SqlStatusClass.Class_3D__Invalid_Catalog_Name },
            { "3F", SqlStatusClass.Class_3F__Invalid_Schema_Name },
            { "40", SqlStatusClass.Class_40__Transaction_Rollback },
            { "42", SqlStatusClass.Class_42__Syntax_Error_or_Access_Rule_Violation },
            { "44", SqlStatusClass.Class_44__WITH_CHECK_OPTION_Violation },
            { "53", SqlStatusClass.Class_53__Insufficient_Resources },
            { "54", SqlStatusClass.Class_54__Program_Limit_Exceeded },
            { "55", SqlStatusClass.Class_55__Object_Not_In_Prerequisite_State },
            { "57", SqlStatusClass.Class_57__Operator_Intervention },
            { "58", SqlStatusClass.Class_58__System_Error_external_to_PostgreSQL_itself },
            { "72", SqlStatusClass.Class_72__Snapshot_Failure },
            { "F0", SqlStatusClass.Class_F0__Configuration_File_Error },
            { "HV", SqlStatusClass.Class_HV__Foreign_Data_Wrapper_Error_SQL_MED },
            { "P0", SqlStatusClass.Class_P0__PLpgSQL_Error },
            { "XX", SqlStatusClass.Class_XX__Internal_Error }
        });

        public static SqlStatusClass GetSqlStatusClass(string sqlStatus)
        {
            if (!string.IsNullOrEmpty(sqlStatus) && sqlStatus.Length >= 2)
            {
                var classPrefix = sqlStatus.Substring(0, 2);
                return _sqlStatusClassPrefix.ContainsKey(classPrefix) ? _sqlStatusClassPrefix[classPrefix] : SqlStatusClass.Class_Unknow;
            }
            else
            {
                return SqlStatusClass.Class_Unknow;
            }
        }
    }
}
